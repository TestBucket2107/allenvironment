
resource "aws_vpc" "singaporeVPC" {
  cidr_block       = var.cidr_block
  instance_tenancy = var.instance_tenancy
  enable_dns_hostnames = true
  tags = {
    Name = var.VPC_name
  }
}

resource "aws_subnet" "singaporeSubnet" {
  vpc_id     = aws_vpc.singaporeVPC.id
  cidr_block = var.cidr_block_subnet

  tags = {
    Name = "singaporeSubnet"
  }
}
