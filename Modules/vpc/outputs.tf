output "vpc_id" {
  value = aws_vpc.singaporeVPC.id
}

output "subnet_id" {
  value = aws_subnet.singaporeSubnet.id
}

