provider "aws" {
  region = "ap-southeast-1"
  version = "~> 2.59"
}

module "Singapore-VPC" {
  source            = "../Modules/vpc"
  cidr_block        = "20.0.0.0/16"
  instance_tenancy  = "default"
  cidr_block_subnet = "20.0.1.0/24"
  vpc_id = module.Singapore-VPC.vpc_id
  VPC_name = "StageVPC"
}

module "Singapore-Instances" {
  source        = "../Modules/ec2"
  ami           = "ami-0b8cf0f359b1335e1"
  instance_type = "t2.micro"
  subnetID      = module.Singapore-VPC.subnet_id
  ec2_count = 1
  Instance_name = "Stage"
}